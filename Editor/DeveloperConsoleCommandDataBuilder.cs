﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;
using VKTools.DeveloperConsole.Runtime;

namespace VKTools.DeveloperConsole.Editor
{
    public class DeveloperConsoleCommandDataBuilder : IPreprocessBuildWithReport
    {
        public int callbackOrder { get; }
        private readonly List<DeveloperConsoleCommand> _developerConsoleCommands =
            new List<DeveloperConsoleCommand>();
        
        public void OnPreprocessBuild(BuildReport report)
        {
            DeveloperConsoleCachedCommandDataSO data;
            var results = AssetDatabase.FindAssets("t:DeveloperConsoleCachedCommandDataSO");

            if(results.Length == 0)
            {
                data =
                    ScriptableObject.CreateInstance<DeveloperConsoleCachedCommandDataSO>();

                AssetDatabase.CreateAsset(data, "Assets/DeveloperConsoleCommandData.asset");
                AssetDatabase.SaveAssets();
            }
            else
            {
                data = AssetDatabase.LoadAssetAtPath<DeveloperConsoleCachedCommandDataSO>(AssetDatabase.GUIDToAssetPath(results.First()));
                data.cachedCommands.Clear();
            }
            
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            const BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance |
                                       BindingFlags.Static;

            foreach (var assembly in assemblies)
            {
                var types = assembly.GetTypes();

                foreach (var type in types)
                {
                    var members = type.GetMethods(flags);

                    foreach (var member in members)
                    {
                        if (member.CustomAttributes.ToArray().Length <= 0) continue;

                        CreateDeveloperConsoleCommand(member, data);
                    }
                }
            }
            EditorUtility.SetDirty(data);
            AssetDatabase.SaveAssets();
        }
        
        private void CreateDeveloperConsoleCommand(MethodInfo member, DeveloperConsoleCachedCommandDataSO data)
        {
            var attribute = member.GetCustomAttribute<DeveloperConsoleCommandAttribute>();
            if (attribute == null) return;
            if (_developerConsoleCommands.Any(cmd => cmd.CommandID == attribute.CommandID)) return;
            
            var command = new DeveloperConsoleCommand(attribute.CommandID,
                attribute.CommandDescription, attribute.MonoTargetType, member);

            _developerConsoleCommands.Add(command);
            data.cachedCommands.Add(new DevelopConsoleCachedCommand(command));
        }
    }
}