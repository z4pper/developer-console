﻿using System;
using System.Linq;
using System.Reflection;

namespace VKTools.DeveloperConsole.Runtime
{
    public class DeveloperConsoleCommand
    {
        public string CommandID { get; }
        public string CommandDescription { get; }
        public string CommandFormat { get; }
        public MonoTargetType MonoTargetType { get; } 
        public MethodBase CommandMethodBase { get; }
        public ParameterInfo[] CommandParameterTypes { get; }
        public Type CommandClassType { get; }

        public DeveloperConsoleCommand(string commandID, string commandDescription, MonoTargetType monoTargetType, MethodBase commandMethodBase)
        {
            CommandID = commandID;
            CommandDescription = commandDescription;
            MonoTargetType = monoTargetType;
            CommandMethodBase = commandMethodBase;
            CommandClassType = commandMethodBase.DeclaringType;
            CommandParameterTypes = commandMethodBase.GetParameters();
            CommandFormat = CreateCommandFormat();
        }

        private string CreateCommandFormat()
        {
            var result = $"{CommandID} ";
            CommandParameterTypes.ToList().ForEach(parameter =>
            {
                if(parameter.ParameterType == typeof(float)) result += $"<float> ";
                else result += $"<{parameter.ParameterType.Name}> ";
            });
            if (MonoTargetType == MonoTargetType.Registry) result += "<ID>";
            return result.TrimEnd();
        }
    }
}