using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Object = UnityEngine.Object;

namespace VKTools.DeveloperConsole.Runtime
{
    public class DeveloperConsole : MonoBehaviour
    {
        [Header("References")] [SerializeField]
        private TMP_InputField consoleInput;

        public List<DeveloperConsoleCommand> DeveloperConsoleCommands { get; private set; } =
            new List<DeveloperConsoleCommand>();

        private static DeveloperConsoleView _developerConsoleView;
        
        private void Awake()
        {
            if (EventSystem.current == null)
            {
                Debug.LogWarning("Need Event System in current scene to receive input for Developer Console!");
            }
            
            GetAllMethodsWithDeveloperConsoleAttribute();
            DeveloperConsoleCommands = DeveloperConsoleCommands.OrderBy(cmd => cmd.CommandID).ToList();
            _developerConsoleView = GetComponent<DeveloperConsoleView>();
        }

        private void OnDestroy()
        {
            DeveloperConsoleRegistry.DestroyRegistry();
        }

        private void GetAllMethodsWithDeveloperConsoleAttribute()
        {
#if UNITY_EDITOR
            var extractedMethods = TypeCache.GetMethodsWithAttribute<DeveloperConsoleCommandAttribute>();
            foreach (var member in extractedMethods)
            {
                CreateDeveloperConsoleCommand(member);
            }
#else
            var data = Resources.Load<DeveloperConsoleCachedCommandDataSO>("DeveloperConsoleCommandData");
            if (data == null) throw new Exception("Could not load Developer Console Command Data.");
            data.cachedCommands.ForEach(cmd => 
                DeveloperConsoleCommands.Add(new DeveloperConsoleCommand(cmd.CommandID, cmd.CommandDescription, cmd.MonoTargetType, cmd.CommandMethodBase.methodInfo)));
#endif
        }

        private void CreateDeveloperConsoleCommand(MethodInfo member)
        {
            var attribute = member.GetCustomAttribute<DeveloperConsoleCommandAttribute>();
            if (attribute == null) return;
            if (DeveloperConsoleCommands.Any(cmd => cmd.CommandID == attribute.CommandID))
            {
                WriteToDeveloperConsole(
                    $"The commandID '{attribute.CommandID}' is already in use. Command will be skipped.", true);
                return;
            }
            
            var command = new DeveloperConsoleCommand(attribute.CommandID,
                attribute.CommandDescription, attribute.MonoTargetType, member);

            DeveloperConsoleCommands.Add(command);
        }

        public static void WriteToDeveloperConsole(string text, bool isWarning = false, bool isUserInput = false)
        {
            _developerConsoleView.WriteToDeveloperConsole(text, isWarning, isUserInput);
        }

        public void EvaluateCommand()
        {
            if (consoleInput.text == string.Empty) return;

            consoleInput.ActivateInputField();
            WriteToDeveloperConsole(consoleInput.text, false, true);

            _developerConsoleView.CacheInput(consoleInput.text);

            var commandSplit = consoleInput.text.TrimEnd().Split(' ');
            var command = DeveloperConsoleCommands.FirstOrDefault(cmd => commandSplit[0] == (cmd.CommandID));
            consoleInput.text = "";

            if (command == null)
            {
                WriteToDeveloperConsole($"'{commandSplit[0]}' is not a valid command!", true);
                return;
            }

            var parameters = commandSplit.Skip(1).ToArray();

            if (parameters.Length != command.CommandParameterTypes.Length &&
                command.MonoTargetType != MonoTargetType.Registry)
            {
                WriteToDeveloperConsole(
                    $"Invalid number of parameters for this command! Expected '{command.CommandParameterTypes.Length}', but received '{parameters.Length}'",
                    true);
                return;
            }

            if (parameters.Length - 1 != command.CommandParameterTypes.Length &&
                command.MonoTargetType == MonoTargetType.Registry)
            {
                WriteToDeveloperConsole(
                    $"Invalid number of parameters for this command! Expected '{command.CommandParameterTypes.Length + 1}', but received '{parameters.Length}'. " +
                    $"All MonoTargetType Registry commands should always include an ID at the end.", true);
                return;
            }

            if (command.MonoTargetType == MonoTargetType.Registry) ExecuteRegistryCommand(command, parameters);
            else ExecuteCommand(command, parameters);
        }

        private void ExecuteRegistryCommand(DeveloperConsoleCommand command, IReadOnlyCollection<string> parameters)
        {
            var parsedParameters = GetParametersFromStrings(command, parameters.Take(parameters.Count - 1).ToArray());
            if (parsedParameters == null) return;

            var instance = DeveloperConsoleRegistry.GetRegisteredObjectByTypeAndStringID(command.CommandClassType, parameters.Last());
            
            if (instance == null) return;
            
            command.CommandMethodBase.Invoke(instance.Obj.Target, parsedParameters);
        }

        private void ExecuteCommand(DeveloperConsoleCommand command, IReadOnlyList<string> parameters)
        {
            object[] parsedParameters = null;
            if (parameters.Count != 0)
            {
                parsedParameters = GetParametersFromStrings(command, parameters);
                if (parsedParameters == null) return;
            }

            if (command.CommandMethodBase.IsStatic)
            {
                command.CommandMethodBase.Invoke(null, parsedParameters);
                return;
            }

            if (command.CommandClassType.BaseType != typeof(MonoBehaviour))
            {
                WriteToDeveloperConsole(
                    $"The command could not execute because class '{command.CommandClassType.Name}' is not of Type MonoBehaviour and the Method is non static." +
                    $"Use MonoTargetType Registry to execute methods on non MonoBehaviour targets.", true);
                return;
            }

            var targets = new List<Object>();
            switch (command.MonoTargetType)
            {
                case MonoTargetType.All:
                {
                    targets = FindObjectsOfType(command.CommandClassType, false).ToList();
                    break;
                }
                case MonoTargetType.AllInactive:
                {
                    targets = FindObjectsOfType(command.CommandClassType, true).ToList();
                    break;
                }
                case MonoTargetType.Single:
                {
                    targets.Add(FindObjectOfType(command.CommandClassType, false));
                    break;
                }
                case MonoTargetType.SingleInactive:
                {
                    targets.Add(FindObjectOfType(command.CommandClassType, true));
                    break;
                }
            }

            if (targets.Count == 0 || targets[0] == null)
            {
                WriteToDeveloperConsole(
                    $"The command could not execute because no instance of the class '{command.CommandClassType.Name}' could be found.",
                    true);
                return;
            }

            targets.ForEach(target => command.CommandMethodBase.Invoke(target, parsedParameters));
        }

        private object[] GetParametersFromStrings(DeveloperConsoleCommand command, IReadOnlyList<string> parameters)
        {
            var parsedParameterList = new List<object>();
            for (var i = 0; i < parameters.Count; i++)
            {
                var parsedParam =
                    GetParsedParameterFromString(command.CommandParameterTypes[i].ParameterType, parameters[i]);
                if (parsedParam == null)
                {
                    WriteToDeveloperConsole($"Failed to parse the parameter '{parameters[i]}' as type '{command.CommandParameterTypes[i].ParameterType}'.", true);
                    return null;
                }

                parsedParameterList.Add(parsedParam);
            }

            return parsedParameterList.ToArray();
        }

        private object GetParsedParameterFromString(Type t, string value)
        {
            switch (t)
            {
                case { } when t == typeof(bool):
                {
                    if (bool.TryParse(value, out var b))
                    {
                        return b;
                    }

                    return null;
                }
                case { } when t == typeof(double):
                {
                    if (double.TryParse(value, out var d))
                    {
                        return d;
                    }

                    return null;
                }
                case { } when t == typeof(float):
                {
                    if (float.TryParse(value.Replace(',', '.'), out var f))
                    {
                        return f;
                    }

                    break;
                }
                case { } when t == typeof(int):
                {
                    if (int.TryParse(value, out var i))
                    {
                        return i;
                    }

                    break;
                }
                case { } when t == typeof(string):
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        return value;
                    }

                    break;
                }
                case { } when t == typeof(Vector3):
                {
                    value = value.Replace("(", "").Replace(")"," ");
                    string[] s = value.Split(',');

                    if (!float.TryParse(s[0], out var x)) break;
                    if (!float.TryParse(s[1], out var y)) break;
                    if (!float.TryParse(s[2], out var z)) break;
               
                    return new Vector3(x,y,z);
                }                
                case { } when t == typeof(Vector2):
                {
                    value = value.Replace("(", "").Replace(")"," ");
                    string[] s = value.Split(',');

                    if (!float.TryParse(s[0], out var x)) break;
                    if (!float.TryParse(s[1], out var y)) break;
                    
                    return new Vector2(x,y);
                }
                case { } when t == typeof(GameObject):
                {
                    return GameObject.Find(value);
                }
                case { } when t == typeof(Type):
                {
                    var type = Type.GetType(value);
                    
                    return type ?? DeveloperConsoleRegistry.GetTypeByStringID(value);
                }
                default:
                {
                    var type = DeveloperConsoleRegistry.GetTypeByStringID(t.Name);
                    if (type != null) return DeveloperConsoleRegistry.GetRegisteredObjectByTypeAndStringID(type, value)?.Obj.Target;
                    
                    WriteToDeveloperConsole($"The type '{t}' is not supported nor is it registered in the registry.", true);
                    break;
                }
            }

            return null;
        }
    }

    public enum MonoTargetType
    {
        All,
        AllInactive,
        Single,
        SingleInactive,
        Registry
    }
}