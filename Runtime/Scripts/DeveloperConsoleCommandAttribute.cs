using System;

namespace VKTools.DeveloperConsole.Runtime
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class DeveloperConsoleCommandAttribute : Attribute
    {
        public string CommandID { get; }
        public string CommandDescription { get; }
        public MonoTargetType MonoTargetType { get; }

        public DeveloperConsoleCommandAttribute(string commandID, string commandDescription, MonoTargetType monoTargetType = MonoTargetType.Single)
        {
            CommandID = commandID;
            CommandDescription = commandDescription;
            MonoTargetType = monoTargetType;
        }
    }
}