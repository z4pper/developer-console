﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace VKTools.DeveloperConsole.Runtime
{
    [Serializable]
    public class DeveloperConsoleCachedCommandDataSO : ScriptableObject
    {
        [SerializeField] public List<DevelopConsoleCachedCommand> cachedCommands = new List<DevelopConsoleCachedCommand>();
    }

    [Serializable]
    public class DevelopConsoleCachedCommand
    {
        public string CommandID;
        public string CommandDescription;
        public MonoTargetType MonoTargetType;
        public SerializableMethodBase CommandMethodBase;

        public DevelopConsoleCachedCommand(DeveloperConsoleCommand command)
        {
            CommandID = command.CommandID;
            CommandDescription = command.CommandDescription;
            MonoTargetType = command.MonoTargetType;
            CommandMethodBase = new SerializableMethodBase(command.CommandMethodBase);
        }
    }
}