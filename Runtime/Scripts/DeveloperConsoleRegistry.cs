﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace VKTools.DeveloperConsole.Runtime
{
    public static class DeveloperConsoleRegistry
    {
        private static readonly Dictionary<RegisteredType, List<RegisteredObject>> allTargetsOfType =
            new Dictionary<RegisteredType, List<RegisteredObject>>();

        private static uint instanceIDCounter;
        private static uint typeIDCounter;

        public static void RegisterObject(object target)
        {
            if (allTargetsOfType.Keys.Any(key => key.Type == target.GetType() || target.GetType().IsSubclassOf(key.Type)))
            {
                var value = allTargetsOfType[allTargetsOfType.Keys.First(key => key.Type == target.GetType() || target.GetType().IsSubclassOf(key.Type))];

                if (value.Any(ro => ro.Obj.Target == target))
                {
                    DeveloperConsole.WriteToDeveloperConsole($"Tried to register the object '{target}' twice. Skipping...", true);
                    return;
                }
            
                value.Add(new RegisteredObject(target, instanceIDCounter));
            }
            else
            {
                var list = new List<RegisteredObject>() {new RegisteredObject(target, instanceIDCounter)};
                var type = new RegisteredType(target.GetType(), typeIDCounter);
                allTargetsOfType.Add(type, list);
                typeIDCounter++;
            }

            instanceIDCounter++;
            if (instanceIDCounter == uint.MaxValue)
                throw new Exception("Developer Console Registry ID overflow. No IDs left for instances.");
            if (typeIDCounter == uint.MaxValue)
                throw new Exception("Developer Console Registry ID overflow. No IDs left for types.");
        }

        public static void DeregisterObject(object target)
        {
            if (allTargetsOfType.TryGetValue(allTargetsOfType.Keys.First(key => key.Type == target.GetType() || target.GetType().IsSubclassOf(key.Type)),
                out var value))
            {
                value.RemoveAll(ro => ro.Obj.Target == target);
            }
            else
            {
                DeveloperConsole.WriteToDeveloperConsole($"Could not find the type {target.GetType()}", true);
            }
        }

        public static void DestroyRegistry()
        {
            allTargetsOfType.Clear();
            instanceIDCounter = 0;
            typeIDCounter = 0;
        }

        public static RegisteredObject GetRegisteredObjectByTypeAndStringID(Type type, string id)
        {
            if (!allTargetsOfType.TryGetValue(allTargetsOfType.Keys.First(key => key.Type == type || type.IsSubclassOf(key.Type)), out var value))
            {
                DeveloperConsole.WriteToDeveloperConsole($"Could not find any values of type {type}", true);
                return null;
            }

            var registeredObject = int.TryParse(id, out var parsedID) ? value.Find(ro => ro.Id == parsedID) : value.Find(ro => ro.nameID == id);
            
            if (registeredObject == null)
                DeveloperConsole.WriteToDeveloperConsole($"Could not find object of type {type} with the ID or Name: {id}",
                    true);

            else if (registeredObject.Obj.Target == null)
            {
                DeveloperConsole.WriteToDeveloperConsole(
                    $"The object of type {type} with the ID: {id} no longer exists. Make sure to deregister objects when destroyed. " +
                    $"Removing this object from the list...", true);
                value.Remove(registeredObject);
                registeredObject = null;
            }

            return registeredObject;
        }
        
        public static Type GetTypeByStringID(string id)
        {
            if (int.TryParse(id, out var value))
            {
                var result = allTargetsOfType.Keys.FirstOrDefault(key => key.Id == value);
                if (result != null) return result.Type;

                DeveloperConsole.WriteToDeveloperConsole($"The ID: {value} is not associated with any type.", true);
                return null;
            }

            var rt = allTargetsOfType.Keys.FirstOrDefault(key => key.nameID == id);
            if (rt != null) return rt.Type;
            
            rt = allTargetsOfType.Keys.FirstOrDefault(key => key.Type.FullName == id);
            if (rt != null) return rt.Type;

            DeveloperConsole.WriteToDeveloperConsole($"The Name: {id} is not associated with any type.", true);
            return null;
        }

        private static List<RegisteredObject> GetAllObjectsOfType(Type type)
        {
            if (allTargetsOfType.TryGetValue(allTargetsOfType.Keys.First(key => key.Type == type || type.IsSubclassOf(key.Type)), out var value))
                return value;

            DeveloperConsole.WriteToDeveloperConsole($"Could not find any values of type {type}", true);
            return null;
        }

        [DeveloperConsoleCommand("print", "Prints all registered objects of a specific type.")]
        private static void PrintRegisteredObjectsOfType(Type t)
        {
            var objs = GetAllObjectsOfType(t);
            if (objs == null)
            {
                DeveloperConsole.WriteToDeveloperConsole($"Nothing to print for type {t}", true);
                return;
            }

            DeveloperConsole.WriteToDeveloperConsole($"<b>-----------------OBJECTS OF TYPE {t.Name.ToUpper()}-----------------</b>");

            objs.ForEach(obj =>
            {
                DeveloperConsole.WriteToDeveloperConsole($"<b>ID:</b> <color=#E25600><b>{obj.Id}</b></color>   <b>Name:</b> <color=#E25600><b>{obj.nameID}</b></color>   <b>Type:</b> {obj.Obj.Target.GetType()}");
            });
        }

        [DeveloperConsoleCommand("printTypes", "Prints all registered types.")]
        private static void PrintAllAvailableTypes()
        {
            DeveloperConsole.WriteToDeveloperConsole("<b>------------------------------TYPES------------------------------</b>");
            allTargetsOfType.Keys.ToList().ForEach(key =>
                DeveloperConsole.WriteToDeveloperConsole($"<b>ID:</b> <color=blue><b>{key.Id}</b></color>   <b>Name:</b> <color=blue><b>{key.nameID}</b></color>   <b>Type:</b> {key.Type}"));
        }
    }

    public class RegisteredObject
    {
        public WeakReference Obj { get; }
        public uint Id { get; }
        public string nameID { get; }

        public RegisteredObject(object o, uint id)
        {
            Obj = new WeakReference(o);
            Id = id;

            var pos = o.ToString().LastIndexOf(" ", StringComparison.Ordinal);
            nameID = pos == -1 ? o.ToString() : o.ToString().Substring(0, pos);
            nameID = Regex.Replace( nameID, @"\s+", "" );
        }
    }

    public class RegisteredType
    {
        public Type Type { get; }
        public uint Id { get; }
        public string nameID { get; }

        public RegisteredType(Type type, uint id)
        {
            Type = type;
            Id = id;
            nameID = type.Name;
        }
    }
}