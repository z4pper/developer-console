﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace VKTools.DeveloperConsole.Runtime
{
    public class DeveloperConsoleView : MonoBehaviour
    {
        [Header("Settings")] 
        [SerializeField, Range(1, 50)] private int maxPreviousInputBufferSize = 15;
        [SerializeField, Range(1, 500)] private int maxAmountOfTextElements = 50;
        [SerializeField] private Color warningTextColor = Color.black;

        [Header("References")] [SerializeField]
        private DeveloperConsole developerConsole;

        [SerializeField] private TMP_InputField consoleInput;
        [SerializeField] private List<Button> suggestionButtons;
        [SerializeField] private TextMeshProUGUI syntaxHelper;
        [SerializeField] private GameObject suggestionParent;
        [SerializeField] private GameObject textRowPrefab;
        [SerializeField] private GameObject consoleParent;
        [SerializeField] private Transform outputContentTransform;
        [SerializeField] private ScrollRect scrollRect;

        public static event Action OnConsoleOpened;
        public static event Action OnConsoleClosed;

        private readonly List<DeveloperConsoleCommand> _commandSuggestions = new List<DeveloperConsoleCommand>();
        private readonly List<TextMeshProUGUI> _textElements = new List<TextMeshProUGUI>();
        private readonly List<Text> _suggestionButtonTexts = new List<Text>();
        private readonly List<string> _previousInputs = new List<string>();
        private int _currentInputBufferSizeIndex = int.MaxValue;
        private int _currentTextElementIndex;
        private bool _isPreviousInputActive;


        private void Awake()
        {
            for (var i = 0; i < maxAmountOfTextElements; i++)
            {
                _textElements.Add(Instantiate(textRowPrefab, outputContentTransform).GetComponent<TextMeshProUGUI>());
            }

            suggestionButtons.ForEach(btn => _suggestionButtonTexts.Add(btn.GetComponentInChildren<Text>()));
        }
        
        public void WriteToDeveloperConsole(string text, bool isWarning = false, bool isUserInput = false)
        {
            var textElement = _textElements[_currentTextElementIndex];
            textElement.transform.SetAsLastSibling();
            _currentTextElementIndex++;
            if (_currentTextElementIndex >= maxAmountOfTextElements) _currentTextElementIndex = 0;

            if (isUserInput)
            {
                textElement.text = text;
            }
            else
            {
                textElement.text = "> " + text;
            }

            if (isWarning)
            {
                textElement.color = warningTextColor;
                textElement.fontStyle = FontStyles.Italic;
            }
            else
            {
                textElement.color = Color.black;
                textElement.fontStyle = FontStyles.Normal;
            }

            Canvas.ForceUpdateCanvases();
            scrollRect.verticalNormalizedPosition = 0f;
        }

        public void CacheInput(string text)
        {
            if (_previousInputs.Contains(text)) _previousInputs.Remove(text);
            _previousInputs.Insert(0, text);
            _currentInputBufferSizeIndex = int.MaxValue;
            if (_previousInputs.Count > maxPreviousInputBufferSize) _previousInputs.RemoveAt(_previousInputs.Count - 1);
        }

        public void CloseConsole()
        {
            if(!consoleParent.activeSelf) return;
            
            ToggleConsole();
        }

        public void ToggleConsole()
        {
            consoleParent.SetActive(!consoleParent.activeSelf);
            if (consoleParent.activeSelf)
            {
                InvokeConsoleOpenEvent();
                consoleInput.Select();
                consoleInput.ActivateInputField();
            }
            else
            {
                InvokeConsoleCloseEvent();
            }
        }

        public void DisplayPreviousUserInputForward()
        {
            if (suggestionParent.activeSelf || _previousInputs.Count == 0) return;

            if (_currentInputBufferSizeIndex == int.MaxValue) _currentInputBufferSizeIndex = 0;
            else if (++_currentInputBufferSizeIndex >= _previousInputs.Count) _currentInputBufferSizeIndex = 0;
            
            _isPreviousInputActive = true;
            consoleInput.text = _previousInputs[_currentInputBufferSizeIndex];
            consoleInput.MoveTextEnd(false);
        }

        public void DisplayPreviousUserInputBackward()
        {
            if (suggestionParent.activeSelf || _previousInputs.Count == 0) return;
            
            if (_currentInputBufferSizeIndex == int.MaxValue) _currentInputBufferSizeIndex = _previousInputs.Count - 1;
            else if (--_currentInputBufferSizeIndex < 0) _currentInputBufferSizeIndex = _previousInputs.Count - 1;
           
            _isPreviousInputActive = true;
            consoleInput.text = _previousInputs[_currentInputBufferSizeIndex];
            consoleInput.MoveTextEnd(false);
        }

        public void ResetPreviousInputsIndex()
        {
            if (!_isPreviousInputActive) _currentInputBufferSizeIndex = int.MaxValue;
        }

        public void ShowCommandSuggestions()
        {
            if (_isPreviousInputActive)
            {
                _isPreviousInputActive = false;
                return;
            }

            _suggestionButtonTexts.ForEach(txt => txt.text = "");
            suggestionButtons.ForEach(btn => btn.gameObject.SetActive(false));

            for (var i = 0; i < _commandSuggestions.Count; i++)
            {
                _suggestionButtonTexts[i].text = _commandSuggestions[i].CommandID;
                suggestionButtons[i].gameObject.SetActive(true);
            }

            suggestionParent.SetActive(_commandSuggestions.Count > 0);
        }

        public void SelectCommandSuggestion(Text txt)
        {
            consoleInput.text = txt.text;
            consoleInput.Select();
            consoleInput.MoveTextEnd(false);
        }

        public void UpdateSyntaxHelperText()
        {
            _commandSuggestions.Clear();
            if (consoleInput.text == "")
            {
                syntaxHelper.text = "";
                return;
            }

            foreach (var cmd in developerConsole.DeveloperConsoleCommands)
            {
                if (cmd.CommandID.StartsWith(consoleInput.text, StringComparison.OrdinalIgnoreCase))
                {
                    _commandSuggestions.Add(cmd);
                    if (_commandSuggestions.Count == _suggestionButtonTexts.Count) break;
                }
            }

            syntaxHelper.text = _commandSuggestions.Count > 0 ? _commandSuggestions.First().CommandFormat : "";
            if (syntaxHelper.text != "")
                consoleInput.text = _commandSuggestions.First().CommandFormat.Substring(0, consoleInput.text.Length);
        }

        public void MoveToCommandSuggestion()
        {
            if (!consoleInput.isFocused) return;
            
            suggestionButtons.First().Select();
        }

        public void AutoCompleteCommandSuggestion()
        {
            if(!suggestionParent.activeSelf) return;

            consoleInput.text = _suggestionButtonTexts.First().text;
            consoleInput.MoveTextEnd(false);
        }

        private void InvokeConsoleOpenEvent()
        {
            OnConsoleOpened?.Invoke();
        }

        private void InvokeConsoleCloseEvent()
        {
            OnConsoleClosed?.Invoke();
        }

        [DeveloperConsoleCommand("help", "Prints all commands registered at the console.")]
        private void PrintAllCommands()
        {
            developerConsole.DeveloperConsoleCommands.ForEach(cmd =>
                WriteToDeveloperConsole($"{cmd.CommandFormat} - {cmd.CommandDescription}"));
            if (developerConsole.DeveloperConsoleCommands.Count > maxAmountOfTextElements)
                WriteToDeveloperConsole(
                    $"There are more commands ({developerConsole.DeveloperConsoleCommands.Count}) than available text elements ({maxAmountOfTextElements}). Increase size in the inspector.",
                    true);
        }

        [DeveloperConsoleCommand("clear", "Clears the console.")]
        private void ClearConsole()
        {
            _textElements.ForEach(txt => txt.text = "");
        }
    }
}