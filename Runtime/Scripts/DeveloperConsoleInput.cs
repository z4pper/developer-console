﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace VKTools.DeveloperConsole.Runtime
{
    public class DeveloperConsoleInput : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private KeyCode additionalKeyToOpenConsole;
        
        [Header("References")] [SerializeField]
        private DeveloperConsoleView developerConsoleView;

        private DeveloperConsoleInputActions _developerConsoleInputActions;

        private void Awake()
        {
            _developerConsoleInputActions = new DeveloperConsoleInputActions();
            _developerConsoleInputActions.Enable();
            
            _developerConsoleInputActions.DeveloperConsoleMap.ToggleConsole.performed += ToggleConsole;
            _developerConsoleInputActions.DeveloperConsoleMap.CloseConsole.performed += CloseConsole;
            _developerConsoleInputActions.DeveloperConsoleMap.NavigateCommandSuggestions.performed += MoveToCommandSuggestion;
            _developerConsoleInputActions.DeveloperConsoleMap.AutoCompleteCommandSuggestion.performed += AutoCompleteCommandSuggestion;
            _developerConsoleInputActions.DeveloperConsoleMap.DisplayPreviousUserInputForward.performed += DisplayPreviousUserInputForward;
            _developerConsoleInputActions.DeveloperConsoleMap.DisplayPreviousUserInputBackward.performed += DisplayPreviousUserInputBackward;

            if (additionalKeyToOpenConsole != KeyCode.None)
            {
                _developerConsoleInputActions.DeveloperConsoleMap.ToggleConsole.AddBinding("<Keyboard>/" + additionalKeyToOpenConsole);
            }
        }

        private void OnDestroy()
        {
            _developerConsoleInputActions.DeveloperConsoleMap.ToggleConsole.performed -= ToggleConsole;
            _developerConsoleInputActions.DeveloperConsoleMap.CloseConsole.performed -= CloseConsole;
            _developerConsoleInputActions.DeveloperConsoleMap.NavigateCommandSuggestions.performed -= MoveToCommandSuggestion;
            _developerConsoleInputActions.DeveloperConsoleMap.AutoCompleteCommandSuggestion.performed -= AutoCompleteCommandSuggestion;
            _developerConsoleInputActions.DeveloperConsoleMap.DisplayPreviousUserInputForward.performed -= DisplayPreviousUserInputForward;
            _developerConsoleInputActions.DeveloperConsoleMap.DisplayPreviousUserInputBackward.performed -= DisplayPreviousUserInputBackward;
        }

        private void ToggleConsole(InputAction.CallbackContext context)
        {
            developerConsoleView.ToggleConsole();
        }

        private void CloseConsole(InputAction.CallbackContext context)
        {
            developerConsoleView.CloseConsole();
        }

        private void MoveToCommandSuggestion(InputAction.CallbackContext context)
        {
            developerConsoleView.MoveToCommandSuggestion();
        }

        private void AutoCompleteCommandSuggestion(InputAction.CallbackContext context)
        {
            developerConsoleView.AutoCompleteCommandSuggestion();
        }

        private void DisplayPreviousUserInputForward(InputAction.CallbackContext context)
        {
            developerConsoleView.DisplayPreviousUserInputForward();
        }
        
        private void DisplayPreviousUserInputBackward(InputAction.CallbackContext context)
        {
            developerConsoleView.DisplayPreviousUserInputBackward();
        }
    }
}