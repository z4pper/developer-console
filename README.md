# **Developer Console**
This package includes a console window for development purposes. It allows printing and executing of methods at runtime.

## **Usage**
This package allows for easy integration into your unity project. Simply add this package via git URL 
and place the "DeveloperConsoleCanvas" Prefab in your scene. Now you are able to open the developer console
window with the "backquote" key (english) or "caret" key (german).

To add commands to the application you have to use the `DeveloperConsoleCommand` attribute at the desired methods.
The following parameters are present:
- `commandID` the identifier of the command, this will be used to call and execute the command
- `commandDescription` a brief description what the command does, this will show when using the command `help`
- `monoTargetType` defines on which type of instance the command will be executed and if all or a single instance should be used

## **Mono Target Types**
The default value for this parameter is `MonoTargetType.Single`. The types define the used methods for finding the instances
to execute the commands on. The type `All` uses `FindObjectsOfType` and the type `Single` uses `FindObjectOfType`.
- `MonoTargetType.All` executes the command on all active instances of the defined type
- `MonoTargetType.AllInactive` executes the command on all active and inactive instances of the defined type
- `MonoTargetType.Single` executes the command on the first active instance of the defined type
- `MonoTargetType.SingleInactive` executes the command on the first active or inactive instance of the defined type
- `MonoTargetType.Registry` registers a specific instance to allow the execution of the command on it

The `Registry` type works a bit different. It is used to register a specific instance and later execute the commands on that
instance. For this to work, the instance has to use `DeveloperConsoleRegistry.RegisterObject` once for registration and 
`DeveloperConsoleRegistry.DeregisterObject` for deregistration.

## **Predefined Commands**
- `help` prints all commands to the console window
- `clear` clears the console window
- `printTypes` prints all registered types to the console window
- `print <Type>` prints all instances of the given type with their ID's 



